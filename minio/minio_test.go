package minio

import (
	"context"
	"testing"
)

var cli = &Client{
	Host:      "127.0.0.1:9000",
	AccessKey: "test123456",
	Secret:    "test123456",
	Bucket:    "test",
}

//var cli = &Client{
//	Host:      "8.130.142.242:9001",
//	AccessKey: "MQPEROH8SJJBJGGDXGNF",
//	Secret:    "fjZwIjNg7fOhncWhNGWjE37mwaQVzPt1BJ5KS3WD",
//	Bucket:    "neuronet",
//}

func TestRead(t *testing.T) {
	bytes, err := cli.GetNeuroObjectBytes(context.Background(), "/aaa.txt")
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(string(bytes))
}

func TestListObject(t *testing.T) {
	err := cli.RemoveObjects(context.Background(), "project")
	if err != nil {
		t.Error(err)
	}
}
