package minio

import (
	"context"
	"github.com/minio/minio-go/v7"
)

func (store *Client) ListObjects(ctx context.Context, prefix string) (<-chan minio.ObjectInfo, error) {
	minioClient, err := newMinioClient(store)
	if err != nil {
		return nil, err
	}

	return minioClient.ListObjects(ctx, store.Bucket, minio.ListObjectsOptions{
		Prefix:    prefix,
		Recursive: true,
	}), nil

}
