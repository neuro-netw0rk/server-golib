package minio

import (
	"bytes"
	"context"
	"io"

	"github.com/minio/minio-go/v7"
)

func (store *Client) GetNeuroObject(ctx context.Context, filePath string) (obj *minio.Object, err error) {
	minioClient, err := newMinioClient(store)
	if err != nil {
		return nil, err
	}

	obj, err = minioClient.GetObject(ctx, store.Bucket, filePath, minio.GetObjectOptions{})
	if err != nil {
		return nil, err
	}

	return
}

func (store *Client) GetNeuroObjectBytes(ctx context.Context, filePath string) (buffer []byte, err error) {
	minioClient, err := newMinioClient(store)
	if err != nil {
		return nil, err
	}

	obj, err := minioClient.GetObject(ctx, store.Bucket, filePath, minio.GetObjectOptions{})
	if err != nil {
		return []byte{}, err
	}
	var reader = bytes.NewBuffer(buffer)
	if _, err = io.Copy(reader, obj); err != nil {
		return buffer, err
	}

	return reader.Bytes(), nil
}
