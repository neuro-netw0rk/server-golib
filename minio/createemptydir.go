package minio

import (
	"bytes"
	"context"
)

func (store *Client) CreateEmptyDir(ctx context.Context, filePath string) error {
	reader := bytes.NewReader([]byte(""))

	_, err := store.UploadStream(ctx, filePath, reader, reader.Size(), "")
	if err != nil {
		return err
	}

	return nil
}
