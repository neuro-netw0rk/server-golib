package minio

import (
	"archive/zip"
	"context"
	"fmt"
	"io"

	"github.com/minio/minio-go/v7"
)

func (store *Client) Upload(ctx context.Context, objectName, filePath, contentType string, metadata ...map[string]string) (minio.UploadInfo, error) {
	minioClient, err := newMinioClient(store)
	if err != nil {
		return minio.UploadInfo{}, err
	}

	options := makePutObjectOptions(contentType, metadata...)

	// 使用FPutObject上传一个zip文件。
	return minioClient.FPutObject(
		ctx, store.Bucket, objectName, filePath, *options)
}

func (store *Client) UploadStream(ctx context.Context, objectName string, r io.Reader, objectSize int64, contentType string, metadata ...map[string]string) (minio.UploadInfo, error) {
	minioClient, err := newMinioClient(store)
	if err != nil {
		return minio.UploadInfo{}, err
	}

	options := makePutObjectOptions(contentType, metadata...)
	options.DisableMultipart = objectSize > 0

	// 使用PutObject上传一个reader流。
	return minioClient.PutObject(ctx, store.Bucket, objectName, r, objectSize, *options)
}

func makePutObjectOptions(contentType string, metadata ...map[string]string) *minio.PutObjectOptions {
	options := minio.PutObjectOptions{
		ContentType:      contentType,
		UserMetadata:     make(map[string]string),
		DisableMultipart: true,
	}

	for _, meta := range metadata {
		for k, v := range meta {
			options.UserMetadata[k] = v
		}
	}

	return &options
}

func MinIOExtractToDir(c context.Context, minioClient *Client, sourceBucket string, sourceFilePath string, targetBucket string, targetFilePath string) error {

	minioClient.Bucket = sourceBucket

	object, err := minioClient.GetNeuroObject(c, sourceFilePath)
	stat, _ := object.Stat()

	if err != nil {
		return err
	}
	read, err := zip.NewReader(object, stat.Size)
	if err != nil {
		return err
	}

	minioClient.Bucket = targetBucket
	// Iterate through the files in the zip archive
	for _, f := range read.File {
		filePath := fmt.Sprintf("%s/code/%s", targetFilePath, f.Name)
		// Open the current file
		v, err := f.Open()
		fileInfo := f.FileInfo()
		defer v.Close()
		if err != nil {
			return err
		}
		_, err = minioClient.UploadStream(c, filePath, v, fileInfo.Size(), "")
		if err != nil {
			return err
		}
	}
	return nil
}
