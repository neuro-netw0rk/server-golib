package minio

import (
	"context"
	"time"

	"github.com/minio/minio-go/v7"
)

func (store *Client) RemoveObject(ctx context.Context, objectName string) error {
	minioClient, err := newMinioClient(store)
	if err != nil {
		return err
	}

	newCTX, _ := context.WithTimeout(ctx, 3*time.Second)
	return minioClient.RemoveObject(newCTX, store.Bucket, objectName, minio.RemoveObjectOptions{ForceDelete: true, GovernanceBypass: true})
}

func (store *Client) RemoveObjects(ctx context.Context, filePath string) error {

	ch, err := store.ListObjects(ctx, filePath)
	if err != nil {
		return err
	}
	for obj := range ch {
		err = store.RemoveObject(ctx, obj.Key)
		if err != nil {
			return err
		}
	}
	return nil
}
