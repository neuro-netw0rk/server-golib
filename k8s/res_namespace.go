package k8s

import (
	"context"

	"gitee.com/neuro-netw0rk/server-servicegolib/code"

	"gitee.com/neuro-netw0rk/server-golib/errors"
	"gitee.com/neuro-netw0rk/server-golib/k8s/informer"
	"k8s.io/client-go/kubernetes"

	"k8s.io/api/core/v1"
)

var _ INamespaceAction = (*namespaces)(nil)

type INamespace interface {
	Namespaces(clusterName string) INamespaceAction
}

type INamespaceAction interface {
	List(ctx context.Context) ([]*v1.Namespace, error)
}

type namespaces struct {
	client   kubernetes.Interface
	informer informer.Storer
}

func newNamespaces(c kubernetes.Interface, informerStore informer.Storer) *namespaces {
	return &namespaces{
		client:   c,
		informer: informerStore,
	}
}

func (c *namespaces) List(ctx context.Context) ([]*v1.Namespace, error) {
	if c.informer == nil {
		return nil, errors.WithCode(code.ErrClusterNotFound, "informer is nil")
	}
	return c.informer.InformerNamespaces().List(ctx)
}
